import { Button, Image, Layout, notification, Space, Typography } from 'antd';
import React, { useState } from 'react';
import './App.css';

function App() {

  const [walletAddress, setWalletAddress] = useState(null);

  async function connectWallet() {

    const { solana } = window;

    if (solana) {
      const response = await solana.connect();
      console.log('Connected wallet with Public Key:', response.publicKey.toString());
      setWalletAddress(response.publicKey.toString());
    } else {
      notification.open({ message: 'Solana object not found!', description: "Get a Phantom Wallet 👻" });
    }
  };

  async function disconnectWallet() {

    const { solana } = window;

    if (solana) {
      await solana.disconnect();
      console.log('Wallet disconnected');
      setWalletAddress(null);
      notification.open({ message: '👋 Bye Bye, my dear!', description: "I'll be waiting for you." });
    } else {
      notification.open({ message: 'Solana object not found!', description: "Get a Phantom Wallet 👻" });
    }
  }

  function renderNotConnectedContainer() {
    return (
      <Space direction='vertical' align='center' size='large' className='content-container'>
        <Image src="https://media.giphy.com/media/8TweEdaxxfuElKkRxz/giphy-downsized-large.gif" preview={false} width={"100%"} />
        <Typography.Text className='text-title'>Present your account to enter the Bunker!</Typography.Text>
        <Button type='primary' size='large' onClick={connectWallet}>Connect Account</Button>
      </Space>
    )
  }

  function renderConnectedContainer() {
    return (
      <Space direction='vertical' align='center' size='large' className='content-container'>
        <Image src="https://media.giphy.com/media/YoPyBaMYzfoSjNRu3G/giphy.gif" preview={false} width={"100%"} />
        <Typography.Text className='text-title'>Do you like being in my Bunker?</Typography.Text>
        <Typography.Text className='text-description'>My dear friend with an account: <b>{walletAddress.substring(0, 6)}...{walletAddress.substring(walletAddress.length - 4)}</b>.</Typography.Text>
        <Button type='primary' size='large' onClick={disconnectWallet}>Disconnect Account and Run Away</Button>
      </Space>
    )
  }

  return (
    <Layout>
      {!walletAddress && renderNotConnectedContainer()}
      {walletAddress && renderConnectedContainer()}
    </Layout>
  );
}

export default App;
