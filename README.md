# Commands

- `npm install` - Install node modules
- `npm start` - Lauch local server
- `vercel --prod` - Deploy to Vercel

# Links

- Repository - https://bitbucket.org/kiv1n/bunker/src/master/
- Website - https://bunker-nine.vercel.app/